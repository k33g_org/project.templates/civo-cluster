#!/bin/bash
set -o allexport; source .env; set +o allexport

civo apikey add civo-key ${CIVO_API_KEY}
civo apikey current civo-key
civo kubernetes create ${CLUSTER_NAME} --size=${CLUSTER_SIZE} --nodes=${CLUSTER_NODES} --region=${CLUSTER_REGION} --wait

# Cluster size g3.k3s.medium / g3.k3s.large / g3.k3s.xlarge 